#include "HttpServer.h"
#include "RemoteParamDevice.h"
#include <QFileDialog>
#include <QMetaObject>
#include <QDebug>
#include <QTimer>

#include <Neptune.h>
#include <Platinum.h>


HttpServer::HttpServer(QObject *contentProvider)
	: contentProvider(contentProvider)
	, wasPolled(false)
{
	server.RegisterHandler(this);
}

void HttpServer::Run()
{
	upnp.reset(new PLT_UPnP);
	PLT_DeviceHostReference device(new RemoteParamDevice("Dev1"));
	upnp->AddDevice(device);
	update();

	server.Run();
}

void HttpServer::update()
{
	if (!upnp.isNull())
		upnp->Start();
	//QTimer::singleShot(5000, this, SLOT(update()));
}

QVariant HttpServer::TimeSinceLastPoll() const
{
	time_t currentTime = time(0);
	return difftime(currentTime, lastPollTime);
}

void HttpServer::Handle(WebToolkit::HttpServerContext* context)
{
	QVariant pageContent;
	QMetaObject::invokeMethod(contentProvider, "generatePage", Qt::BlockingQueuedConnection,
		Q_RETURN_ARG(QVariant, pageContent));
	context->responseBody << pageContent.toString().toLatin1().data();
	wasPolled = true;
	lastPollTime = time(0);
}

void HttpServer::SaveToFile()
{
	QString fileName = QFileDialog::getSaveFileName(0, "Save parameters", QString(), "JSON file (*.json)");
	if (fileName.isEmpty())
		return;
	QVariant pageContent;
	QMetaObject::invokeMethod(contentProvider, "generatePage", Q_RETURN_ARG(QVariant, pageContent));
	QFile file(fileName);
	file.open(QFile::WriteOnly | QFile::Text);
	QTextStream stream(&file);
	stream << pageContent.toString();
}

Q_INVOKABLE QVariant HttpServer::LoadFromFile()
{
	QString fileName = QFileDialog::getOpenFileName(0, "Load parameters", QString(), "JSON file (*.json)");
	if (fileName.isEmpty())
		return "";
	QFile file(fileName);
	if (!file.exists())
		return "";
	file.open(QFile::ReadOnly | QFile::Text);
	QTextStream stream(&file);
	return stream.readAll();
}
