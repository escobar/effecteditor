#pragma once

#include "PltDeviceHost.h"

class RemoteParamDevice : public PLT_DeviceHost
{
public:
	RemoteParamDevice(const char* FriendlyName, const char* UUID = "19703712-a81f-49cd-821d-cb5b6e765a39");
    virtual ~RemoteParamDevice();

    virtual NPT_Result SetupServices();
};
