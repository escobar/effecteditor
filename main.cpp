#include <QtGui/QGuiApplication>
#include <QQmlContext>
#include <QQuickItem>
#include "qtquick2applicationviewer.h"
#include "HttpServer.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	QtQuick2ApplicationViewer viewer;
	//viewer.setResizeMode(QQuickView::SizeViewToRootObject);
    viewer.setMainQmlFile(QStringLiteral("qml/EffectEditor/main.qml"));
	viewer.showExpanded();
	HttpServer server(viewer.rootObject());
	viewer.rootContext()->setContextProperty("server", &server);
	server.Run();

    return app.exec();
}
