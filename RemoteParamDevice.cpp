#include <NptTypes.h>
#include <Platinum.h>
#include "RemoteParamDevice.h"

extern const char* SCPDXML_SIMPLE;

RemoteParamDevice::RemoteParamDevice(const char* FriendlyName, const char* UUID) :	
	PLT_DeviceHost("/", UUID, "urn:arello-mobile.com:device:RemoteParamController:1", FriendlyName, true)
{
	//SetBroadcast(true);
	SetByeByeFirst(true);
}

RemoteParamDevice::~RemoteParamDevice()
{
}

NPT_Result RemoteParamDevice::SetupServices()
{
	PLT_Service* service = new PLT_Service(
        this,
        "urn:arello-mobile.com:service:RemoteParam:1", 
        "urn:arello-mobile.com:serviceId:RemoteParam",
        "Remote Params Control Service");
    NPT_CHECK_FATAL(service->SetSCPDXML((const char*)SCPDXML_SIMPLE));
    NPT_CHECK_FATAL(AddService(service));

	service->SetStateVariable("Status", "True");

    return NPT_SUCCESS;
}

const char* SCPDXML_SIMPLE =
    "<?xml version=\"1.0\" ?>"
    "  <scpd xmlns=\"urn:schemas-upnp-org:service-1-0\">"
    "    <specVersion>"
    "       <major>1</major>"
    "	    <minor>0</minor>"
    "	 </specVersion>"
    "    <serviceStateTable>"
    "      <stateVariable sendEvents=\"yes\">"
    "        <name>LastChange</name>"
    "        <dataType>string</dataType>"
    "        <defaultValue></defaultValue>"
    "      </stateVariable>"
    "      <stateVariable sendEvents=\"yes\">"
    "        <name>PresetNameList</name>"
    "        <dataType>string</dataType>"
    "        <defaultValue></defaultValue>"
    "      </stateVariable>"
    "    </serviceStateTable>"
    "    <intel_nmpr:X_INTEL_NMPR xmlns:intel_nmpr=\"udn:schemas-intel-com:device-1-0\">2.1</intel_nmpr:X_INTEL_NMPR>"
    "    <dlna:X_DLNADOC xmlns:dlna=\"udn:schemas-dlna-org:device-1-0\">DMP 1.00</dlna:X_DLNADOC>"
    "  </scpd>";
