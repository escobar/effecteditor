# Add more folders to ship with the application, here
folder_01.source = qml/EffectEditor
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

QT += widgets

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

LIBS += -lws2_32

INCLUDEPATH += WebToolkit Neptune PlatinumKit

INCLUDEPATH += Neptune/Win32

HEADERS += \
	Neptune/Win32/NptWin32MessageQueue.h \
	Neptune/Win32/NptWin32Network.h \
	Neptune/Win32/NptWin32Threads.h

SOURCES += \
	Neptune/StdC/NptStdcEnvironment.cpp \
	Neptune/StdC/NptStdcFile.cpp \
	Neptune/Posix/NptPosixFile.cpp \
	Neptune/Bsd/NptBsdSockets.cpp \
	Neptune/Win32/NptWin32Console.cpp \
	Neptune/Win32/NptWin32Debug.cpp \
	Neptune/Win32/NptWin32DynamicLibraries.cpp \
	Neptune/Win32/NptWin32File.cpp \
	Neptune/Win32/NptWin32HttpProxy.cpp \
	Neptune/Win32/NptWin32MessageQueue.cpp \
	Neptune/Win32/NptWin32Network.cpp \
	Neptune/Win32/NptWin32Queue.cpp \
	Neptune/Win32/NptWin32SerialPort.cpp \
	Neptune/Win32/NptWin32System.cpp \
	Neptune/Win32/NptWin32Threads.cpp \
	Neptune/Win32/NptWin32Time.cpp

HEADERS += \
	Neptune/Neptune.h \
	Neptune/NptArray.h \
	Neptune/NptAutomaticCleaner.h \
	Neptune/NptAutoreleasePool.h \
	Neptune/NptBase64.h \
	Neptune/NptBufferedStreams.h \
	Neptune/NptCommon.h \
	Neptune/NptConfig.h \
	Neptune/NptConsole.h \
	Neptune/NptConstants.h \
	Neptune/NptCrypto.h \
	Neptune/NptDataBuffer.h \
	Neptune/NptDebug.h \
	Neptune/NptDefs.h \
	Neptune/NptDigest.h \
	Neptune/NptDynamicCast.h \
	Neptune/NptDynamicLibraries.h \
	Neptune/NptFile.h \
	Neptune/NptHash.h \
	Neptune/NptHttp.h \
	Neptune/NptInterfaces.h \
	Neptune/NptJson.h \
	Neptune/NptList.h \
	Neptune/NptLogging.h \
	Neptune/NptMap.h \
	Neptune/NptMessaging.h \
	Neptune/NptNetwork.h \
	Neptune/NptQueue.h \
	Neptune/NptReferences.h \
	Neptune/NptResults.h \
	Neptune/NptRingBuffer.h \
	Neptune/NptSelectableMessageQueue.h \
	Neptune/NptSerialPort.h \
	Neptune/NptSimpleMessageQueue.h \
	Neptune/NptSockets.h \
	Neptune/NptStack.h \
	Neptune/NptStreams.h \
	Neptune/NptStrings.h \
	Neptune/NptSystem.h \
	Neptune/NptThreads.h \
	Neptune/NptTime.h \
	Neptune/NptTls.h \
	Neptune/NptTlsDefaultTrustAnchorsBase.h \
	Neptune/NptTlsDefaultTrustAnchorsExtended.h \
	Neptune/NptTypes.h \
	Neptune/NptUri.h \
	Neptune/NptUtils.h \
	Neptune/NptVersion.h \
	Neptune/NptXml.h \
	Neptune/NptZip.h \
	PlatinumKit/PltAction.h \
	PlatinumKit/PltArgument.h \
	PlatinumKit/PltConstants.h \
	PlatinumKit/PltCtrlPoint.h \
	PlatinumKit/PltCtrlPointTask.h \
	PlatinumKit/PltDatagramStream.h \
	PlatinumKit/PltDeviceData.h \
	PlatinumKit/PltDeviceHost.h \
	PlatinumKit/PltEvent.h \
	PlatinumKit/PltHttp.h \
	PlatinumKit/PltHttpClientTask.h \
	PlatinumKit/PltHttpServer.h \
	PlatinumKit/PltHttpServerTask.h \
	PlatinumKit/PltMimeType.h \
	PlatinumKit/PltProtocolInfo.h \
	PlatinumKit/PltService.h \
	PlatinumKit/PltSsdp.h \
	PlatinumKit/PltStateVariable.h \
	PlatinumKit/PltTaskManager.h \
	PlatinumKit/PltThreadTask.h \
	PlatinumKit/PltUPnP.h \
	PlatinumKit/PltUtilities.h \
	WebToolkit/Client.h \
	WebToolkit/File.h \
	WebToolkit/FileUtils.h \
	WebToolkit/Http.h \
	WebToolkit/HttpHelpers.h \
	WebToolkit/Logger.h \
	WebToolkit/OSUtils.h \
	WebToolkit/Server.h \
	WebToolkit/Socket.h \
	WebToolkit/Stream.h \
	WebToolkit/Thread.h \
	WebToolkit/Util.h \
	WebToolkit/WebToolkit.h \
	HttpServer.h \
	RemoteParamDevice.h

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += \
	Neptune/Neptune.cpp \
	Neptune/NptAutomaticCleaner.cpp \
	Neptune/NptBase64.cpp \
	Neptune/NptBufferedStreams.cpp \
	Neptune/NptCommon.cpp \
	Neptune/NptConsole.cpp \
	Neptune/NptCrypto.cpp \
	Neptune/NptDataBuffer.cpp \
	Neptune/NptDebug.cpp \
	Neptune/NptDigest.cpp \
	Neptune/NptDynamicLibraries.cpp \
	Neptune/NptFile.cpp \
	Neptune/NptHash.cpp \
	Neptune/NptHttp.cpp \
	Neptune/NptJson.cpp \
	Neptune/NptList.cpp \
	Neptune/NptLogging.cpp \
	Neptune/NptMessaging.cpp \
	Neptune/NptNetwork.cpp \
	Neptune/NptQueue.cpp \
	Neptune/NptResults.cpp \
	Neptune/NptRingBuffer.cpp \
	Neptune/NptSimpleMessageQueue.cpp \
	Neptune/NptSockets.cpp \
	Neptune/NptStreams.cpp \
	Neptune/NptStrings.cpp \
	Neptune/NptSystem.cpp \
	Neptune/NptThreads.cpp \
	Neptune/NptTime.cpp \
	Neptune/NptTls.cpp \
	Neptune/NptUri.cpp \
	Neptune/NptUtils.cpp \
	Neptune/NptXml.cpp \
	Neptune/NptZip.cpp \
	PlatinumKit/PltAction.cpp \
	PlatinumKit/PltArgument.cpp \
	PlatinumKit/PltConstants.cpp \
	PlatinumKit/PltCtrlPoint.cpp \
	PlatinumKit/PltCtrlPointTask.cpp \
	PlatinumKit/PltDatagramStream.cpp \
	PlatinumKit/PltDeviceData.cpp \
	PlatinumKit/PltDeviceHost.cpp \
	PlatinumKit/PltEvent.cpp \
	PlatinumKit/PltHttp.cpp \
	PlatinumKit/PltHttpClientTask.cpp \
	PlatinumKit/PltHttpServer.cpp \
	PlatinumKit/PltHttpServerTask.cpp \
	PlatinumKit/PltIconsData.cpp \
	PlatinumKit/PltMimeType.cpp \
	PlatinumKit/PltProtocolInfo.cpp \
	PlatinumKit/PltService.cpp \
	PlatinumKit/PltSsdp.cpp \
	PlatinumKit/PltStateVariable.cpp \
	PlatinumKit/PltTaskManager.cpp \
	PlatinumKit/PltThreadTask.cpp \
	PlatinumKit/PltUPnP.cpp \
	WebToolkit/Client.cpp \
	WebToolkit/File.cpp \
	WebToolkit/FileUtils.cpp \
	WebToolkit/Http.cpp \
	WebToolkit/HttpHelpers.cpp \
	WebToolkit/Logger.cpp \
	WebToolkit/OSUtils.cpp \
	WebToolkit/Server.cpp \
	WebToolkit/Socket.cpp \
	WebToolkit/Stream.cpp \
	WebToolkit/Thread.cpp \
	WebToolkit/Util.cpp \
	RemoteParamDevice.cpp \
	HttpServer.cpp \
	main.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()
