import QtQuick 2.0

Column {
	id: rangedValueEditor
	width: 400
	property real max: 1.0
	property real min: 0.0
	property alias value: slider.value
	property alias name: nameText.text
	property int fracDigits: 4
	property string valueStr: value.toFixed(fracDigits)
	property real normalizedValue: parseFloat(valueStr)

	Row {
		spacing: 8
		Text {
			id: nameText
			text: "Name"
			font.pointSize: 12
		}
		Text {
			text: ":"
			font.pointSize: 12
		}
		Text {
			text: rangedValueEditor.valueStr
			font.pointSize: 12
		}
	}
	Slider {
		id: slider
		anchors.right: parent.right
		anchors.left: parent.left
		minimum: rangedValueEditor.min
		maximum: rangedValueEditor.max
	}
}
