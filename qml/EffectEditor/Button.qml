import QtQuick 2.0

MouseArea {
	property alias name: text.text
	width: 150
	height: 60
	Rectangle {
		id: rect
		color: parent.pressed ? "gray" : "lightgray"
		anchors.fill: parent
		Text {
			id: text
			anchors.centerIn: parent
			color: "white"
			font.pointSize: 12
		}
	}
}
