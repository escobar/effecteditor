import QtQuick 2.0

Rectangle {
	function generatePage() {
		console.log("generatePage invoked")
		var result = {
			specular_power: specularPowerEditor.normalizedValue,
			specular_intencity: specularIntencityEditor.normalizedValue,
			ambient_intencity: ambientIntencityEditor.normalizedValue,
			light_direction: lightDirectionEditor.normalizedValue,
			view_direction: viewDirectionEditor.normalizedValue,
		}
		return JSON.stringify(result);
	}

	id: root
	width: 360
	height: 640

	Flickable {
		anchors.fill: parent
		contentHeight: content.height + content.y * 2

		Timer {
			interval: 500
			repeat: true
			running: true
			onTriggered: {
				if (server.TimeSinceLastPoll() > 3.0)
					root.color = "#FFF0F0"
				else
					root.color = "#F0FFF0"
			}
		}
		Column {
			id: content
			anchors.right: parent.right
			anchors.left: parent.left
			anchors.rightMargin: 8
			anchors.leftMargin: 8
			y: 8
			spacing: 2
			Row {
				spacing: 8
				Button {
					width: 150
					height: 60
					name: "Save to file"
					onClicked: server.SaveToFile()
				}
				Button {
					width: 150
					height: 60
					name: "Load from file"
					visible: false
					onClicked: {
						var fileContent = server.LoadFromFile()
						if (fileContent === "")
							return
						var rootObj = JSON.parse(fileContent)
						specularPowerEditor.value = rootObj.specular_power
						specularIntencityEditor.value = rootObj.specular_intencity
						ambientIntencityEditor.value = rootObj.ambient_intencity
						//lightDirectionEditor.setValue(rootObj.light_direction)
						//viewDirectionEditor.setValue(rootObj.view_direction)
					}
				}
			}

			RangedValueEditor {
				id: specularPowerEditor
				anchors.right: parent.right
				anchors.left: parent.left
				name: "Specular power"
				min: 2
				max: 256
				value: 32
				fracDigits: 0
			}
			RangedValueEditor {
				id: specularIntencityEditor
				anchors.right: parent.right
				anchors.left: parent.left
				name: "Specular intencity"
				min: 0
				max: 1
				value: 0.2
				fracDigits: 4
			}
			RangedValueEditor {
				id: ambientIntencityEditor
				anchors.right: parent.right
				anchors.left: parent.left
				name: "Ambient intencity"
				min: 0
				max: 1
				value: 0.8
				fracDigits: 4
			}
			VectorEditor {
				id: lightDirectionEditor
				name: "Light direction"
				fracDigits: 4
			}
			VectorEditor {
				id: viewDirectionEditor
				name: "View direction"
				fracDigits: 4
			}
		}
	}
}
