import QtQuick 2.0

Column {
	function getNormalizedVector(x, y, z) {
		var l = Math.sqrt(x * x + y * y + z * z)
		return [
					parseFloat((x / l).toFixed(fracDigits)),
					parseFloat((y / l).toFixed(fracDigits)),
					parseFloat((z / l).toFixed(fracDigits)),]
	}
	function getNormalizedVectorStr(x, y, z) {
		var l = Math.sqrt(x * x + y * y + z * z)
		return (x / l).toFixed(fracDigits)
				+ " " + (y / l).toFixed(fracDigits)
				+ " " + (z / l).toFixed(fracDigits)
	}

	id: vectorEditor
	property alias name: nameText.text
	property int fracDigits: 4
	property var normalizedValue: getNormalizedVector(dragArea.xAxis, dragArea.yAxis, -slider.value)
	/*property string valueStr: dragArea.xAxis.toFixed(fracDigits)
							  + " " + dragArea.yAxis.toFixed(fracDigits)
							  + " " + (-slider.value).toFixed(fracDigits)*/
	property string normalizedValueStr: getNormalizedVectorStr(dragArea.xAxis, dragArea.yAxis, -slider.value)
	property real quadBound: 200
	Row {
		spacing: 8
		Text {
			id: nameText
			text: "Name"
			font.pointSize: 12
		}
		Text {
			text: ":"
			font.pointSize: 12
		}
		Text {
			text: normalizedValueStr
			font.pointSize: 12
		}
	}
	Row {
		spacing: 4
		Rectangle {
			id: lookPosEditor
			width: vectorEditor.quadBound
			height: vectorEditor.quadBound
			color: "lightgray"
			Row {
				anchors.bottom: parent.bottom
				anchors.left: parent.left
				spacing: 8
				Text {
					text: "Axis"
					font.pointSize: 7
					color: "gray"
				}
				Text {
					text: ":"
					font.pointSize: 7
					color: "gray"
				}
				Text {
					text: dragArea.xAxis.toFixed(fracDigits)
						  + " " + dragArea.yAxis.toFixed(fracDigits)
						  + " " + slider.value.toFixed(fracDigits)
					font.pointSize: 7
					color: "gray"
				}
			}
			Rectangle {
				id: dragArea
				property real xAxis: 2 * x / mouseArea.drag.maximumX - 1
				property real yAxis: 1 - 2 * y / mouseArea.drag.maximumY
				radius: 10
				width: radius * 2
				height: width
				color: "gray"
				smooth: true
				x: mouseArea.drag.maximumX / 2
				y: mouseArea.drag.maximumY / 2
				MouseArea {
					id: mouseArea
					anchors.fill: parent
					drag.target: parent
					drag.axis: Drag.XAndYAxis
					drag.minimumX: 0
					drag.minimumY: 0
					drag.maximumX: lookPosEditor.width - parent.width
					drag.maximumY: lookPosEditor.height - parent.height
				}
			}
		}
		Item {
			id: sliderRoot
			width: slider.height
			height: slider.width
			Slider {
				id: slider
				x: parent.width
				width: lookPosEditor.height
				rotation: 90
				transformOrigin: Item.TopLeft
				minimum: -1
				maximum: -0.0001
				value: -1
			}
		}
	}
}
