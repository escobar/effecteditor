#pragma once

#include <WebToolkit/WebToolkit.h>
#include <QVariant>
#include <QObject>
#include <ctime>
#include <Platinum.h>

class HttpServer
	: public QObject
	, public WebToolkit::HttpHandler
{
	Q_OBJECT

public:
	HttpServer(QObject *contentProvider);

	void Run();
	void Handle(WebToolkit::HttpServerContext* context);
	Q_INVOKABLE QVariant TimeSinceLastPoll() const;
	Q_INVOKABLE void SaveToFile();
	Q_INVOKABLE QVariant LoadFromFile();

protected slots:
	void update();

private:
	WebToolkit::Server server;
	QObject *contentProvider;
	time_t lastPollTime;
	bool wasPolled;
	QScopedPointer<PLT_UPnP> upnp;
};
